package com.lubinskiy.app01.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.lubinskiy.app01.gitProjects.GitLabProject;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<GitLabProject> mGitLabProjects;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView text;

        private ViewHolder(View v) {
            super(v);
            text = v.findViewById(android.R.id.text1);
        }
    }

    public RecyclerViewAdapter(List<GitLabProject> data) {
        this.mGitLabProjects = data;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        GitLabProject gitLabProject = mGitLabProjects.get(position);
        holder.text.setText(gitLabProject.getName());
    }

    @Override
    public int getItemCount() {
        return mGitLabProjects.size();
    }
}
