package com.lubinskiy.app01.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import com.lubinskiy.app01.gitAPI.GitLabAPI;
import com.lubinskiy.app01.R;
import com.lubinskiy.app01.adapter.RecyclerViewAdapter;
import com.lubinskiy.app01.gitProjects.GitLabProject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProjectsActivity extends AppCompatActivity implements View.OnClickListener {

    private String mUsername;
    private String mToken;

    private EditText mSearchEditText;
    private RecyclerView mRecyclerView;

    private GitLabAPI mGitLabAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects_layout);
        initialization();

        mUsername = getIntent().getStringExtra("username");
        mToken = getIntent().getStringExtra("private_token");

        createGitLabApi();
        mGitLabAPI.getProjects(mUsername, mToken).enqueue(callbackGetProjects);

    }

    private void initialization() {
        mSearchEditText = findViewById(R.id.editText_search);
        mRecyclerView = findViewById(R.id.list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(ProjectsActivity.this));
    }

    private void createGitLabApi() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GitLabAPI.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mGitLabAPI = retrofit.create(GitLabAPI.class);
    }

    @Override
    public void onClick(View v) {
        mGitLabAPI.searchProjects(mUsername, mSearchEditText.getText().toString(),
                mToken).enqueue(callbackGetProjects);
    }

    Callback<List<GitLabProject>> callbackGetProjects =
            new Callback<List<GitLabProject>>() {
                @Override
                public void onResponse(Call<List<GitLabProject>> call,
                                       Response<List<GitLabProject>> response) {
                    if (response.isSuccessful()) {
                        List<GitLabProject> gitLabProject = response.body();
                        mRecyclerView.setAdapter(new RecyclerViewAdapter(gitLabProject));
                    } else {
                        Toast.makeText(ProjectsActivity.this, "Code: " +
                                        response.code() + " Message: " + response.message(),
                                        Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<GitLabProject>> call, Throwable t) {
                    t.getMessage();
                }
            };
}